


VERSION=2.7.4
# install apache
#
echo "################# WASC Honeypot Installer ####################"
echo "#"
echo "#  (1) Installing required packages  (Apache, Apache-dev tools)"
echo "#"
apt-get install apache2 apache2-threaded-dev >> installer.log


# install prerequisites for compiling+installing modsecurity
#
echo "#  (2) Installing required libraries (libcurl, libxml,...)"
echo "#"
apt-get install make libxml2-dev libcurl4-openssl-dev liblua5.1-0-dev >> installer.log

#
# fetch, build and install a recent modsecurity
#

echo "#  (3) Fetching tarball from modsecurity.org..."
echo "#"
rm -f modsecurity.tar.gz
wget -q -O modsecurity.tar.gz https://www.modsecurity.org/tarball/$VERSION/modsecurity-apache_$VERSION.tar.gz

echo "#  (4) Unpacking modsecurity tarball..."
echo "#"
tar xf modsecurity.tar.gz

cd modsecurity-apache_$VERSION

echo "#  (5) Configuring ModSecurity $VERSION"
echo "#"
./configure --prefix=/opt/modsecurity >> installer.log

echo "#  (6) Building and installing ModSecurity..."
echo "#"
make && make install >> installer.log
