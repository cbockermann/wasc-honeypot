The WASC Honeypot Installer
===========================

The Web Application Security Consortium manages a central collector
for ModSecurity honeypot data. Participants are asked to set up honeypots
based on ModSecurity and the OWASP Core Ruleset and send their data to
the central log collector.

This project provides an easy-to-use shell script for the instant setup of
a honeypot system based on a recent Ubuntu machine.


Installing a Honeypot
---------------------

When installing a honeypot, all you have to do is to start with a bare
Ubuntu server machine, installing `git` and checkout the *wasc-honeypot-installer*
to get your sensor up and running:

     # sudo apt-get install git-core
     # sudo git clone https://bitbucket.org/cbockermann/wasc-honeypot.git
     # cd wasc-honeypot
     # sudo sh wasc-honeypot-installer.sh


What is included
----------------

The *wasc-honeypot-installer* script installs the required packages for
Apache and Apache development (`apache2-threaded-dev`) and fetches the
most recent ModSecurity tarball from the ModSecurity web site. The script
unpacks the tarball, configures and compiles the ModSecurity Apache module.

The ModSecurity module and all of the basic configuration parts required
for the system to act like a sensor are installed to `/opt/modsecurity`.
